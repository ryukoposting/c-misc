/* Sample code for misc.h */

#include <stdio.h>

#include "misc.h"

int main(int argc, char **argv) {
    // uncommenting the line below will cause a compile-time error.
    //checktype(7, char);
    
    // this also causes a compiler error.
    //int foo = 7;
    //checktype(foo, char);
    
    // beware that checktype cannot differentiate between signed and unsigned.
    
    // demonstration of foreach and filterforeach
    char test_array[] = "hello world!";
    foreach(test_array, {
        printf("foreach %d: %c\n", index, it);
    });
    
    filterforeach(test_array, { index % 2; }, {
        printf("filterforeach %d: %c\n", index, it);
    });
    
    
    // bitreverse. remember- don't pass a function as an argument to an array.
    char foo = 0x12;
    short bar = 0x1234;
    int bat = 0x12345678;
    long baz = 0x0123456789ABCDEF;
    printf("%x\n", foo);
    printf("%x\n", bitreverse(foo));
    printf("%x\n", bar);
    printf("%x\n", bitreverse(bar));
    printf("%x\n", bat);
    printf("%x\n", bitreverse(bat));
    printf("%lx\n", baz);
    printf("%lx\n", bitreverse(baz));
    
}
