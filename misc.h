/**
 * @file misc.h
 * @brief Macro "abuse"
 * 
 * Just some helpful macros, wrapped into a header file. Remember, don't give
 * functions as parameters to macro functions. If you remember that, you'll be fine.
 * 
 * @author ryukoposting
 */
#ifndef MISC_H_
#define MISC_H_

#include <stddef.h>
#include <stdlib.h>

#define __PRAGMA(x) { _Pragma(#x) }

/**
 * @brief Throw a compile-time error if @ref var is not the exact type specified by @ref type.
 * @param var The variable or literal to type-check
 * @param type The correct type for @ref var
 * 
 * This function works inside of other function macros.
 */
#define checktype(var, type) {\
    __PRAGMA(GCC diagnostic push);\
    __PRAGMA(GCC diagnostic error "-Wincompatible-pointer-types");\
    __typeof(var) *__t;\
    __t = (type*)NULL;\
    __PRAGMA(GCC diagnostic pop);\
}

/**
 * @brief Enumerate and iterate through an array.
 * @param array The array that will be iterated.
 * @param _func A pseudo-closure. The pseudo-closure will have access to variables `it` and `index`
 */
#define foreach(array, _func) {\
    unsigned int _array_size = (sizeof(array)/sizeof(array[0]));\
    for(unsigned int _i = 0; _i < _array_size; ++_i) {\
        const __auto_type it = array[_i];\
        const unsigned int index = _i; \
        _func;\
    }\
}

/**
 * @brief Enumerate and iterate through an array.
 * @param array The array that will be iterated.
 * @param _cond A pseudo-closure that should return a boolean. It has access to variables `it` and `index`
 * @param _func A pseudo-closure that runs for each element that returns true for @ref _cond. It has access 
 *              to variables `it` and `index`
 */
#define filterforeach(array, _cond, _func) {\
    unsigned int _array_size = (sizeof(array)/sizeof(array[0]));\
    for(unsigned int _i = 0; _i < _array_size; ++_i) {\
        const __auto_type it = array[_i];\
        const unsigned int index = _i; \
        long int res = ( _cond );\
        if (res)  _func; \
    }\
}

/**
 * @brief Returns the bit-reverse of a char, short, int, or long.
 */
#define bitreverse(_in_val) ({\
    __auto_type out = _in_val;\
    switch (sizeof(out)) {\
        case 8:\
            out = ((out & 0xFFFFFFFF00000000UL) >> 32) | ((out & 0x00000000FFFFFFFFUL) << 32);\
        case 4:\
            out = ((out & 0xFFFF0000FFFF0000UL) >> 16) | ((out & 0x0000FFFF0000FFFFUL) << 16);\
        case 2:\
            out = ((out & 0xFF00FF00FF00FF00UL) >> 8)  | ((out & 0x00FF00FF00FF00FFUL) << 8);\
        case 1:\
            out = ((out & 0xF0F0F0F0F0F0F0F0UL) >> 4)  | ((out & 0x0F0F0F0F0F0F0F0FUL) << 4);\
            out = ((out & 0xCCCCCCCCCCCCCCCCCC) >> 2)  | ((out & 0x333333333333333333) << 2);\
            out = ((out & 0xAAAAAAAAAAAAAAAAAA) >> 1)  | ((out & 0x555555555555555555) << 1);\
            break;\
    }\
    out;\
})

#endif
